import logging
import re
import copy

from game_abstract import GameAbstract
from data_frame import DataFrame
from random import randint


class BattleshipGame(GameAbstract):

    F_EMPTY = 0
    F_SHIP_1 = 1
    F_SHIP_2 = 2
    F_SHIP_3 = 3
    F_SHIP_4 = 4
    F_ATTACKED_EMPTY = 9
    F_ATTACKED_SHIP_PART = 8
    F_ATTACKED_SHIP_FULL = 7

    @classmethod
    def _convert_camelcase(cls, csstring):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', csstring)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

    def on_command(self, input, player):
        if input and "action" in input:
            try:
                method = "on_" + BattleshipGame._convert_camelcase(input["action"]) + "_command"
                if method in dir(BattleshipGame):
                    getattr(self, method)(input=input, player=player)
                else:
                    logging.info("no such method %r in game", method)

            except:
                logging.warning("got exception on command %r", input["action"], exc_info=True)
        else:
            logging.info("wrong command")

    def initialize(self):
        self.player1 = None
        self.player2 = None
        self.state = None

        self.player1 = self.players[list(self.players)[0]]
        self.random_allocate(self.player1)
        if len(self.players) > 1:
            self.player2 = self.players[list(self.players)[1]]
            self.random_allocate(self.player2)
            self.state = "allocate1"

            df = DataFrame("playerIndex", 1)
            self.player1.send(df)
            df = DataFrame("playerIndex", 2)
            self.player2.send(df)

            df = DataFrame("opponent", self.player1)
            self.player2.send(df)

            df = DataFrame("opponent", self.player2)
            self.player1.send(df)
        else:
            df = DataFrame("error", "need 2 players")
            for iplayer in self.players.values():
                iplayer.send(df)

    def clear_battlefield(self, player):
        defaultField = {}
        for i in range(1, 10):
            defaultField[i] = {}
            for j in range(1, 10):
                defaultField[i][j] = self.F_EMPTY

        if player == self.player1:
            self.leftFieldPlayer1 = copy.deepcopy(defaultField)
            self.rightFieldPlayer1 = copy.deepcopy(defaultField)
        elif player == self.player2:
            self.leftFieldPlayer2 = copy.deepcopy(defaultField)
            self.rightFieldPlayer2 = copy.deepcopy(defaultField)

    def random_allocate(self, player):
        self.clear_battlefield(player)

        self.allocate_ship(self.F_SHIP_4, player)
        self.allocate_ship(self.F_SHIP_3, player)
        self.allocate_ship(self.F_SHIP_3, player)
        self.allocate_ship(self.F_SHIP_2, player)
        self.allocate_ship(self.F_SHIP_2, player)
        self.allocate_ship(self.F_SHIP_2, player)
        self.allocate_ship(self.F_SHIP_1, player)
        self.allocate_ship(self.F_SHIP_1, player)
        self.allocate_ship(self.F_SHIP_1, player)
        self.allocate_ship(self.F_SHIP_1, player)

    def allocate_ship(self, size, player):
        field = None
        if player == self.player1:
            field = self.rightFieldPlayer1
        elif player == self.player2:
            field = self.rightFieldPlayer2
        if field:
            allocation = []
            possible_cells = []
            for i in range(1, 10):
                for j in range(1, 10):
                    possible_cells.append([i, j])

            while not len(allocation) == size:
                direction = randint(1, 4)
                possible_cell = possible_cells[randint(0, len(possible_cells) - 1)]
                x = possible_cell[0]
                y = possible_cell[1]
                x_off = 0
                y_off = 0
                if direction == 1:
                    y_off = 1
                elif direction == 2:
                    x_off = 1
                elif direction == 3:
                    y_off = -1
                else:
                    x_off = -1
                for s in range(1, size + 1):
                    i = x + x_off * (s - 1)
                    j = y + y_off * (s - 1)
                    possible = True
                    for np in self.get_nearest_cells(i, j):
                        if np not in allocation and not self.cell_is_empty(field, np[0], np[1]):
                            possible = False
                    if self.cell_is_empty(field, i, j) and possible:
                        allocation.append([i, j])
                    else:
                        allocation = []
                possible_cells.remove(possible_cell)
            for point in allocation:
                field[point[0]][point[1]] = size

    def get_nearest_cells(self, i, j):
        cells = []
        cells.append([i+1, j])
        cells.append([i+1, j+1])
        cells.append([i+1, j-1])
        cells.append([i-1, j])
        cells.append([i-1, j+1])
        cells.append([i-1, j-1])
        cells.append([i, j+1])
        cells.append([i, j-1])

        for cell in cells:
            if (cell[0] < 1) or (cell[1] < 1) or (cell[0] > 9) or (cell[1] > 9):
                cells = list(filter((cell).__ne__, cells))

        return cells

    def cell_is_empty(self, field, i, j):
        if i > 0 and i < 10 and j > 0 and j < 10:
            if field[i][j] == self.F_EMPTY:
                return True
            else:
                return False
        return None

    def attack(self, i, j, attacker):
        battlefield_enemy = None
        battlefield_info = None
        success = False
        if attacker == self.player1:
            battlefield_enemy = self.rightFieldPlayer2
            battlefield_info = self.leftFieldPlayer1
        elif attacker == self.player2:
            battlefield_enemy = self.rightFieldPlayer1
            battlefield_info = self.leftFieldPlayer2

        if battlefield_info and battlefield_enemy:
            attack_cell = battlefield_enemy[i][j]
            if attack_cell == self.F_EMPTY:
                battlefield_enemy[i][j] = self.F_ATTACKED_EMPTY
                battlefield_info[i][j] = self.F_ATTACKED_EMPTY
            elif (attack_cell == self.F_SHIP_1 or attack_cell == self.F_SHIP_2 or
                    attack_cell == self.F_SHIP_3 or
                    attack_cell == self.F_SHIP_4):
                battlefield_enemy[i][j] = self.F_ATTACKED_SHIP_PART
                battlefield_info[i][j] = self.F_ATTACKED_SHIP_PART
                success = True

                full = True
                full_ship_cells = self.get_full_ship_location(i, j, battlefield_enemy, checked_cells=[])
                for ship_cell in full_ship_cells:
                    if battlefield_enemy[ship_cell[0]][ship_cell[1]] != self.F_ATTACKED_SHIP_PART:
                        full = False
                if full:
                    for ship_cell in full_ship_cells:
                        battlefield_enemy[ship_cell[0]][ship_cell[1]] = self.F_ATTACKED_SHIP_FULL
                        battlefield_info[ship_cell[0]][ship_cell[1]] = self.F_ATTACKED_SHIP_FULL
                        for cell in self.get_nearest_cells(ship_cell[0], ship_cell[1]):
                            if battlefield_enemy[cell[0]][cell[1]] == self.F_EMPTY:
                                battlefield_enemy[cell[0]][cell[1]] = self.F_ATTACKED_EMPTY
                                battlefield_info[cell[0]][cell[1]] = self.F_ATTACKED_EMPTY
        return success

    def get_full_ship_location(self, i, j, battlefield, checked_cells=[]):
        location = []

        cells = []
        cells.append([i, j])
        cells.append([i+1, j])
        cells.append([i-1, j])
        cells.append([i, j+1])
        cells.append([i, j-1])

        for cell in cells:
            if cell[0] in battlefield and cell[1] in battlefield[cell[0]] and cell not in checked_cells:
                f = battlefield[cell[0]][cell[1]]
                if (f == self.F_SHIP_1 or f == self.F_SHIP_2 or f == self.F_SHIP_3 or
                        f == self.F_SHIP_4 or f == self.F_ATTACKED_SHIP_PART):
                    checked_cells.append(cell)
                    location.append(cell)
                    location = location + self.get_full_ship_location(i=cell[0], j=cell[1], battlefield=battlefield, checked_cells=checked_cells)
        return location

    def is_possible_to_attack(self, i, j, player):
        possible = False
        if (player == self.player1 and self.state == "turn1" or
                player == self.player2 and self.state == "turn2"):
            if player == self.player1:
                bf = self.leftFieldPlayer1
            elif player == self.player2:
                bf = self.leftFieldPlayer2
            if (i in bf and j in bf[i] and bf[i][j] == self.F_EMPTY):
                possible = True
        return possible

    def calc_damaged_ships(self, battlefield):
        found = 0
        for row in battlefield.values():
            for val in row.values():
                if val == self.F_ATTACKED_SHIP_FULL or val == self.F_ATTACKED_SHIP_PART:
                    found += 1
        return found

    def check_end_game(self):
        damaged1 = self.calc_damaged_ships(self.rightFieldPlayer1)
        damaged2 = self.calc_damaged_ships(self.rightFieldPlayer2)
        end = None
        if damaged1 == 20:
            self.state = "win2"
            end = True
        elif damaged2 == 20:
            self.state = "win1"
            end = True
        if end:
            self.leftFieldPlayer1 = self.rightFieldPlayer2
            self.leftFieldPlayer2 = self.rightFieldPlayer1

    def send_state(self, player):
        data = {"state": self.state, "leftField": {}, "rightField": {}}
        if player == self.player1:
            data["leftField"] = self.leftFieldPlayer1
            data["rightField"] = self.rightFieldPlayer1
        elif player == self.player2:
            data["leftField"] = self.leftFieldPlayer2
            data["rightField"] = self.rightFieldPlayer2
        df = DataFrame("state", data)
        player.send(df)

    def on_get_state_command(self, input, player):
        self.send_state(player)

    def on_random_allocate_command(self, input, player):
        if self.state == "allocate1" or self.state == "allocate2":
            self.random_allocate(player)
            self.send_state(player)

    def on_allocated_command(self, input, player):
        if self.state == "allocate1" and player == self.player1:
            self.state = "allocate2"
        elif self.state == "allocate2" and player == self.player2:
            self.state = "turn1"
        for iplayer in self.players.values():
            self.send_state(iplayer)

    def on_attack_command(self, input, player):
        i = int(input["data"]["i"])
        j = int(input["data"]["j"])
        if (self.is_possible_to_attack(i, j, player)):
            success = self.attack(i, j, attacker=player)
            if not success:
                if self.state == "turn1":
                    self.state = "turn2"
                else:
                    self.state = "turn1"
            self.check_end_game()
            for iplayer in self.players.values():
                self.send_state(iplayer)

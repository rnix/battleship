from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from db_session import DBSession
from sqlalchemy.orm import class_mapper


class DBMixin:
    def session(self):
        return DBSession.get_session()

    @classmethod
    def get_query(cls):
        return DBSession.get_session().query(cls)

    @classmethod
    def find_by_pk(cls, rowid):
        pk = class_mapper(cls).primary_key[0].name
        args = {str(pk): rowid}
        return cls.get_query().filter_by(**args)

import logging

from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
from sqlalchemy import Column, Integer, String
from db_session import DBSession
from DBMixin import *

class User(DBMixin,Base):
    __tablename__ = 'user'
    uid = Column(String, primary_key=True)
    name = Column(String)
    lobby_reg_token = Column(String)
    lobby_reg_token_upd_date = Column(String)

    def flush_reg_token(self):
        self.lobby_reg_token = ""
        self.session().commit()
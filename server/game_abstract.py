import uuid
import logging

class GameAbstract:
    def __init__(self, room):
        self.room = room
        self.players = {}
        self.id = str(uuid.uuid4())

        for iplayer in room.members.values():
            self.add_player(iplayer)

        self.initialize()

    def initialize(self):
        pass

    def add_player(self, player):
        self.players[player.uid] = player

    def remove_player(self, player):
        self.players.pop(player.uid, None)

    def on_later_join_room(self, player):
        return None

    def on_command(self, input, player):
        logging.info("got command %r", input["action"])

#!/usr/bin/env python
#

import logging
import tornado.escape
import tornado.ioloop
import tornado.web
import tornado.websocket
import os.path
import uuid
import re
import datetime

from config import Config
from player import Player
from room import Room
from data_frame import DataFrame
from battleship_game import BattleshipGame as Game


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/lobby", LobbySocketHandler),
        ]
        tornado.web.Application.__init__(self, handlers)


class LobbySocketHandler(tornado.websocket.WebSocketHandler):
    connections = {}
    players = {}
    rooms = {}
    games = {}

    @classmethod
    def _convert_camelcase(cls, csstring):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', csstring)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

    def open(self):
        self.id = str(uuid.uuid4())
        LobbySocketHandler.connections[self.id] = self

    def on_close(self):
        player = LobbySocketHandler.get_player_by_connection(connection=self)
        if player:
            player.disconnected = True
            LobbySocketHandler.close_room_owned_by_player(player)
            LobbySocketHandler.remove_player_from_rooms(player)

        LobbySocketHandler.connections.pop(self.id, None)

    def on_message(self, message):
        logging.info("got message %r", message)
        parsed = tornado.escape.json_decode(message)

        if "action" in parsed:
            method = "_" + "on_" + LobbySocketHandler._convert_camelcase(parsed["action"]) + "_action"
            player = LobbySocketHandler.get_player_by_connection(self)

            if method == "_on_register_action":
                getattr(LobbySocketHandler, method)(data=parsed, connection=self)
            elif player:
                game = LobbySocketHandler.find_game_with_player(player)
                if method in dir(LobbySocketHandler):
                    getattr(LobbySocketHandler, method)(data=parsed, player=player)
                elif game:
                    game.on_command(input=parsed, player=player)
                else:
                    logging.info("there is not method %r", method)
            else:
                logging.info("Trying to call method %r while not registered. Skip.", method)




    @classmethod
    def _on_register_action(cls, data, connection):
        if "uid" in data and "userLobbyToken" in data:
            player = Player(connection, uid=data["uid"])
            model = player.get_model()
            if player and model and model.lobby_reg_token == data["userLobbyToken"]:
                LobbySocketHandler.players[player.uid] = player
                df = DataFrame("registered", player)
                player.send(df)
                model.flush_reg_token()
            else:
                try:
                    df = DataFrame("error", "register_fail")
                    connection.write_message(df.encode())
                except:
                    logging.error("Register error", exc_info=True)

    @classmethod
    def _on_get_rooms_list_action(cls, data, player):
        df = DataFrame("roomsList", LobbySocketHandler.rooms)
        player.send(df)
                    

    @classmethod
    def _on_create_room_action(cls, data, player):
        if not LobbySocketHandler.get_room_owned_by_player(player):
            if "name" in data and data["name"]:
                room = Room(player, data["name"])
                LobbySocketHandler.rooms[room.id] = room
                df = DataFrame("createdRoom", room)
                player.send(df)
                df = DataFrame("joined", room)
                player.send(df)
                LobbySocketHandler.update_clients_rooms_list()

    @classmethod
    def _on_join_room_action(cls, data, player):
        if data["roomId"] in LobbySocketHandler.rooms.keys():
            room = LobbySocketHandler.rooms[data["roomId"]]
            room.add_member(player)
            df = DataFrame("joined", room)
            LobbySocketHandler.update_clients_room_members(room)

            game = LobbySocketHandler.find_game_in_room(room)
            if game:
                game.on_later_join_room(player)
            player.send(df)

            LobbySocketHandler.system_message(room, player.name + " joined.")

    @classmethod
    def _on_chat_message_action(cls, data, player):
        room = LobbySocketHandler.find_room_with_player(player)
        if room and "text" in data and data["text"]:
            chat = {"text": tornado.escape.xhtml_escape(data["text"]), "sender": player, "time": datetime.datetime.now().strftime("%H:%M:%S")}
            df = DataFrame("chatMessage", chat)
            for iplayer in room.members.values():
                iplayer.send(df)

    @classmethod
    def _on_start_game_action(cls, data, player):
        room = LobbySocketHandler.get_room_owned_by_player(player)
        if room:
            game = LobbySocketHandler.find_game_with_player(player)
            if (game):
                LobbySocketHandler.stop_game(game)
            game = Game(room)
            LobbySocketHandler.games[game.id] = game

            game_info = {"id": game.id, "count": len(LobbySocketHandler.games)}
            df = DataFrame("started", game_info)
            for iplayer in game.players.values():
                iplayer.send(df)

            LobbySocketHandler.system_message(room, "Game was started.")
        else:
            df = DataFrame("error", "player_does_not_own_room")
            player.send(df)

    @classmethod
    def _on_leave_room_action(cls, data, player):
        if data["roomId"] in LobbySocketHandler.rooms:
            room = LobbySocketHandler.rooms[data["roomId"]]
            if room.owner_player.uid == player.uid:
                LobbySocketHandler.close_room(room)
            else:
                df = DataFrame("leftRoom", room)
                player.send(df)
                LobbySocketHandler.before_remove_room_member(room, player)
                room.remove_member(player)
                LobbySocketHandler.after_remove_room_member(room, player)
                LobbySocketHandler.update_clients_room_members(room)

    @classmethod
    def _on_leave_game_action(cls, data, player):
        game = LobbySocketHandler.find_game_with_player(player)
        if (game):
            LobbySocketHandler.before_remove_game_player(game, player)
            game.remove_player(player)
            LobbySocketHandler.after_remove_game_player(game, player)







    @classmethod
    def get_player_by_connection(cls, connection):
        for iplayer in LobbySocketHandler.players.values():
            if connection.id == iplayer.connection.id:
                return iplayer

    @classmethod
    def get_room_owned_by_player(cls, player):
        for room in LobbySocketHandler.rooms.values():
            if player.uid == room.owner_player.uid:
                return room

    @classmethod
    def update_clients_rooms_list(cls):
        for iplayer in LobbySocketHandler.players.values():
            if not LobbySocketHandler.is_player_in_room(iplayer):
                LobbySocketHandler._on_get_rooms_list_action(None, iplayer)

    @classmethod
    def update_clients_room_members(cls, room):
        df = DataFrame("updateRoomMembers", room.members)
        for player in room.members.values():
            player.send(df)

    @classmethod
    def find_game_in_room(cls, room):
        for game in LobbySocketHandler.games.values():
            if game.room.id == room.id:
                return game

    @classmethod
    def is_player_in_room(cls, player):
        for room in LobbySocketHandler.rooms.values():
            for uid in room.members.keys():
                if uid == player.uid:
                    return True

        return False

    @classmethod
    def find_room_with_player(cls, player):
        for room in LobbySocketHandler.rooms.values():
            for iplayer in room.members.values():
                if iplayer.uid == player.uid:
                    return room
        return None

    @classmethod
    def find_game_with_player(cls, player):
        for game in LobbySocketHandler.games.values():
            for iplayer in game.players.values():
                if iplayer.uid == player.uid:
                    return game
        return None

    @classmethod
    def remove_player_from_rooms(cls, player):
        for room in LobbySocketHandler.rooms.values():
            for iplayer in list(room.members.values()):
                if (iplayer.uid == player.uid):
                    LobbySocketHandler.before_remove_room_member(room, player)
                    room.remove_member(player)
                    LobbySocketHandler.after_remove_room_member(room, player)
                    LobbySocketHandler.update_clients_room_members(room)

    @classmethod
    def close_room_owned_by_player(cls, player):
        room = LobbySocketHandler.get_room_owned_by_player(player)
        if room:
            LobbySocketHandler.close_room(room)

            for game in LobbySocketHandler.games.values():
                room = game.room
                if not room or room not in LobbySocketHandler.rooms:
                    LobbySocketHandler.stop_game(game)

    @classmethod 
    def close_room(cls, room):
        LobbySocketHandler.before_close_room(room)
        LobbySocketHandler.rooms.pop(room.id, None)
        room = None
        LobbySocketHandler.update_clients_rooms_list()

    @classmethod
    def stop_game(cls, game):
        LobbySocketHandler.before_stop_game(game)
        LobbySocketHandler.games.pop(game.id, None)
        game = None

    @classmethod
    def before_close_room(cls, room):
        game = LobbySocketHandler.find_game_in_room(room)
        if game:
            LobbySocketHandler.stop_game(game)
        df = DataFrame("leftRoom", room)
        for iplayer in room.members.values():
            iplayer.send(df)

    @classmethod
    def before_remove_room_member(cls, room, player):
        game = LobbySocketHandler.find_game_with_player(player)
        if game:
            LobbySocketHandler.before_remove_game_player(game, player)
            game.remove_player(player)
            LobbySocketHandler.after_remove_game_player(game, player)

    @classmethod
    def after_remove_room_member(cls, room, player):
        LobbySocketHandler.system_message(room, player.name + " left room.")
        if len(room.members) == 0:
            LobbySocketHandler.close_room(room)

    @classmethod
    def after_remove_game_player(cls, game, player):
        LobbySocketHandler.system_message(game.room, player.name + " left game.")
        if len(LobbySocketHandler.players) == 0:
            LobbySocketHandler.stop_game(game)

    @classmethod
    def before_remove_game_player(cls, game, player):
        df = DataFrame("leftGame")
        player.send(df)
        df = DataFrame("someOneLeftGame", player)
        for iplayer in game.players.values():
            if iplayer.uid != player.uid:
                iplayer.send(df)

    @classmethod
    def before_stop_game(cls, game):
        df = DataFrame("leftGame")
        for iplayer in game.players.values():
            iplayer.send(df)

    @classmethod
    def system_message(cls, room, text):
        fake_player = {"uid": "0", "name": "Lobby"}
        chat = {"text": tornado.escape.xhtml_escape(text), "sender": fake_player, "time": datetime.datetime.now().strftime("%H:%M:%S")}
        df = DataFrame("chatMessage", chat)
        for iplayer in room.members.values():
            iplayer.send(df)





def main():
    logging.basicConfig(level=logging.DEBUG)
    app = Application()
    port = Config.get_config()["general"]["port"]
    app.listen(port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()

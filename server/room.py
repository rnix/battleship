import uuid
import tornado.escape

class Room:
    def __init__(self, player, name):
        self.owner_player = player
        self.name = tornado.escape.xhtml_escape(name)
        self.members = {}
        self.id = str(uuid.uuid4())
        self.add_member(player)

    def jsonable(self):
        return {"id": self.id, "name": self.name, "members": self.members, "ownerId": self.owner_player.uid, "owner": self.owner_player}

    def add_member(self, player):
        self.members[player.uid] = player

    def remove_member(self, player):
        self.members.pop(player.uid, None)

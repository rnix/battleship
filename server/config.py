import configparser
import os.path


class Config:
    instance = None
    @classmethod
    def get_config(cls):
        if not Config.instance:
            base_config_dir = os.path.dirname(__file__) + '/../config'

            conf = configparser.ConfigParser()
            conf.readfp(open(base_config_dir + '/config.base.ini'))
            conf = dict(conf)

            if os.path.isfile(base_config_dir + '/config.local.ini'):
                env_conf = configparser.ConfigParser()
                env_conf.readfp(open(base_config_dir + '/config.local.ini'))
                env_conf = dict(env_conf)

                for section, values in env_conf.items():
                    conf[section].update(env_conf[section])

            Config.instance = dict(conf)
        return Config.instance

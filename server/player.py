import logging
import tornado.escape
from user import User
from db_session import DBSession

class Player:
    def __init__(self, connection, uid):
        self.connection = connection
        self.uid = uid
        self.model = None
        self.disconnected = False
        self.name = "?"

        try:           
            self.model = User.get_query().filter(User.uid == self.uid).one()
            self.name = tornado.escape.xhtml_escape(self.model.name)
        except:
            logging.error("Error getting user's model", exc_info=True)

    def jsonable(self):
        return {"uid": self.uid, "name": self.name}

    def send(self, data_frame):
        if self.disconnected:
            return None

        try:
            self.connection.write_message(data_frame.encode())
        except:
            logging.error("Error sending message", exc_info=True)

    def get_model(self):
        return self.model

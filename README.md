Battleship
==========================

###Install
Project uses Python 3.3.

1) Tornado  
[http://www.tornadoweb.org/en/stable/](http://www.tornadoweb.org/en/stable/)  
Download, unpack, run  

    python setup.py build
    python setup.py install


2) SQLAlchemy  
[http://www.sqlalchemy.org/download.html](http://www.sqlalchemy.org/download.html)  
Download, unpack, run  

    python setup.py build
    python setup.py install


3) MySQL connector for python  
[http://dev.mysql.com/downloads/connector/python/](http://dev.mysql.com/downloads/connector/python/)  
Download and install

###Run
    python server/server.py
    
### Run in docker
1. `cp docker-compose.dist.yml docker-compose.yml`
2. Optional `cp config/config.base.ini config/config.local.ini`
3. Set permissions (Linux):
```
sudo find public/assets protected/runtime -type d -exec chmod 777 {} \;
```
4. `docker-compose up -d --build`
5. Run migration
```
docker cp other/db.sql $(docker-compose ps -q mysql):/tmp/dump.sql
docker-compose exec mysql sh -c "mysql battleship -ubattleship -pbattleship < /tmp/dump.sql"
docker-compose restart battleship-ws
```

###The MIT License
    The MIT License

    Copyright (C) 2013 Roman Nix

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.



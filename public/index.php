<?php

$sharedConfig = parse_ini_file(__DIR__ . '/../config/config.base.ini', true);
$sharedEnvConfigFile = __DIR__ . '/../config/config.local.ini';
if (file_exists($sharedEnvConfigFile)){
    $sharedEnvConfig = parse_ini_file($sharedEnvConfigFile, true);

    foreach ($sharedEnvConfig as $section=>$values){
        foreach ($values as $name=>$value){
            $sharedConfig[$section][$name] = $value;
        }
    }

    foreach ($sharedConfig as $section => $values) {
        foreach ($values as $name => $value) {
            $value = str_replace('__DIR__', __DIR__ . '/../config', $value);
            if ($value === '1') {
                $value = true;
            } else if ($value === '0') {
                $value = false;
            }
            $sharedConfig[$section][$name] = $value;
        }
    }
}

$yii = $sharedConfig['env']['yii_path'];
defined('YII_DEBUG') or define('YII_DEBUG', $sharedConfig['env']['yii_debug']);
if (isset($sharedConfig['env']['yii_enable_error_handler'])){
    defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', $sharedConfig['env']['yii_enable_error_handler']);
}
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once($yii);

$configDir = dirname(__FILE__) . '/../protected/config';
$config = require_once( $configDir . '/main.php' );

Yii::createWebApplication($config)->run();
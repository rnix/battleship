/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `uid` varchar(32) NOT NULL,
  `name` varchar(90) default NULL,
  `lll_access_token` varchar(32) default NULL,
  `lll_token_expired_in` int(11) default NULL,
  `lll_user_id` int(10) unsigned default NULL,
  `ins_date` timestamp NULL default CURRENT_TIMESTAMP,
  `defeat_num_pve` int(10) unsigned default NULL,
  `defeat_num_pvp` int(10) unsigned default NULL,
  `victory_num_pve` int(10) unsigned default NULL,
  `victory_num_pvp` int(10) unsigned default NULL,
  `lobby_reg_token` varchar(32) default NULL,
  `lobby_reg_token_upd_date` timestamp NULL default NULL,
  PRIMARY KEY  (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_ach`;
CREATE TABLE IF NOT EXISTS `user_ach` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `uid` varchar(32) NOT NULL,
  `aid` int(10) unsigned NOT NULL,
  `ins_date` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `user_fk` (`uid`),
  CONSTRAINT `user_fk` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

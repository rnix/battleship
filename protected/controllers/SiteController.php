<?php

class SiteController extends Controller {

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
    
    public function actionIndex(){
        $this->render('index');
    }
    
    public function actionLogin() {
        
        if (!empty($_GET['error'])){
            throw new CException($_GET['error']);
        }
                
        $authIdentity = Yii::app()->eauth->getIdentity('livelevel');
        $authIdentity->redirectUrl = Yii::app()->user->returnUrl;
        $authIdentity->cancelUrl = $this->createAbsoluteUrl('site/login');

        if ($authIdentity->authenticate()) {
            $identity = new MyEAuthUserIdentity($authIdentity);
            
            if ($identity->authenticate()) {
                $duration = 3600 * 24 * 30; // 30 days
                Yii::app()->user->login($identity, $duration);
                
                // special redirect with closing popup window
                $authIdentity->redirect();
            } else {
                // close popup window and redirect to cancelUrl
                $authIdentity->cancel();
            }
        }

        // Something went wrong, redirect to login page
        $this->redirect(array('site/index'));
    }
    
    public function actionLogout(){
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionAnonLogin(){
        if (Yii::app()->user->isGuest){
            Yii::app()->user->loginAsAnonymous();
        }
        $this->redirect(Yii::app()->user->returnUrl);
    }
    
    public function actionSelectLogin(){
        $this->render('selectLogin');
    }
    
    public function actionPlay() {
        if (Yii::app()->user->isGuest) {
            Yii::app()->user->setReturnUrl(array('site/play'));
            $this->redirect(Yii::app()->user->loginUrl);
        }

        if (defined('YII_DEBUG') && YII_DEBUG) {
            $debug = "true";
        } else {
            $debug = "false";
        }


        $myPackage = array(
            'basePath' => 'application.views.assets.game',
            'js' => array('js/jquery.lobby.js', 'js/connection.js', 'js/game.js',),
            'css' => array('css/game.css'),
            'img' => array(),
            'depends' => array('jquery', 'jquery.ui')
        );

        $packUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias($myPackage['basePath']), false, -1, defined('YII_DEBUG') && YII_DEBUG);
        Yii::app()->clientScript->addPackage('checkersPack', $myPackage)->registerPackage('checkersPack');

        $cs = Yii::app()->getClientScript();
        $cs->registerScript(
                'lobby-init', "
                        var webSocketAddress = '" . Yii::app()->params['shared']['wsAddress'] . "';
                        var siteUid = '" . Yii::app()->user->getModel()->uid . "';
                        var siteUserLobbyToken = '" . Yii::app()->user->getModel()->generateLobbyToken() . "';
                        var siteDebugMode = " . $debug . ";
                     ", CClientScript::POS_BEGIN);

        Yii::app()->clientScript->registerCssFile(
                Yii::app()->clientScript->getCoreScriptUrl() .
                '/jui/css/base/jquery-ui.css'
        );
        
        $this->render('play', array(
            'gameAssetsUrl' => $packUrl,
        ));
    }
}
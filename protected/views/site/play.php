<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name . ' - Play';
?>

<?php

?>

<script>
  var checkersAssetsUrl = "<?=$gameAssetsUrl;?>";
</script>

<div class="row">

    <div class="alert alert-info connection-wait">
        <h3>Происходит подключение</h3>
        <div class="progress progress-striped active">
            <div class="bar" style="width: 100%;"></div>
        </div>
    </div>
    
    <div class="alert alert-danger connection-error" style="display: none;">
        Не удалось подключиться к серверу.
    </div>
    
    <div class="alert alert-error you-lose game-state-message" style="display: none;">
        Вы проиграли!
    </div>
   
    <div class="alert alert-success you-won game-state-message" style="display: none;">
        Вы победили!
    </div>
    
    <div class="alert alert-success game-is-over game-state-message" style="display: none;">
        Игра окончена.
    </div>
    
    <div class="alert alert-danger on-error-container" style="display: none;">
        
    </div>

    
<!--    game-->
<div class="stage-fixed-width">
    <div class="game" style="display: none;">
        <div class="pull-left bf-left">
            <div class="bf-desc">поле врага</div>
            <div class="battlefield enemy-battlefield"></div>
            <div class="bf-turn bf-your-turn" style="display: none;">ваш ход</div>
        </div>
        <div class="pull-left bf-right">
            <div class="bf-desc">ваше поле</div>
            <div class="battlefield my-battlefield"></div>
            <div class="bf-turn bf-enemy-turn" style="display: none;">ход соперника</div>
            <div class="allocate-control" style="display: none;"><button class="btn btn-success allocate-random">Рандом!</button><button class="btn btn-primary allocate-ready">Готов!</button></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!--    end game-->
    
    
    <div>
        <div class="rooms-list-container" style="display: none;"> 
            <h4>Список комнат</h4>
            <em class="empty-rooms-list-message" style="display: none;">ни одной комнаты не создано</em>
            <ul class="rooms-list">
                <li class="room-item-tpl" style="display: none;">
                    <span class="room-item-name">name</span>
                </li>
            </ul>
            
            <input type="text" name="roomName" placeholder="Название новой комнаты"><br><button class="btn createRoom">Создать комнату</button>
        </div>
        
        
        <div class="game-info-container" style="display: none;">
            <br><button class="btn leaveGame">Покинуть игру</button>
        </div>
        
        <div class="room-chat-container" style="display: none;">
            <div class="chat-lines">
                <div class="chat-message chat-message-tpl" style="display: none;"><span class="chat-author">author</span> (<span class="chat-time">time</span>): <span class="chat-text">text</span></div>
            </div>
            <input type="text" class="input-chat-text">
            <button class="btn chat-send">Отправить</button>
        </div>
        
        
        <div class="players-list-container" style="display: none;"> 
            <h4>Список игроков в комнате <span class="room-name"></span></h4>
            <em class="room-is-empty-message" style="display: none;">комната пуста</em>
            <ul class="players-list">
                <li class="player-item-tpl" style="display: none;">
                    <span class="player-item-name">name</span>
                </li>
            </ul>
            
            <br><button class="btn leaveRoom">Покинуть комнату</button>
            <div class="start-game-container" style="display: none;">
                <br><button class="btn startGame">Запустить игру</button>
            </div>
        </div>
        
        
    </div>
    

</div>
<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="hero-unit">
    <h1>Добро пожаловать на <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
    <p>
        Здесь вы сможете сыграть онлайн в морской бой с другом через браузер. Для запуска нужен браузер с <a href="http://caniuse.com/websockets">поддержкой WebSockets</a> (IE 10, Firefox 6+, Chrome 14+, Safari 6+, Opera 12.1+, Chrome for Android, Firefox for Android).
        Кроме того, можете попробовать написать бота для игры за вас. Формат команд и данных вы можете найти в js-файлах.
    </p>
    <p>
        Проект использует php-фрейморк Yii для клиентской части игры и python-фрейморк Tornado для серверной части. Исходный код проекта: <a href="https://bitbucket.org/rnix/battleship">https://bitbucket.org/rnix/battleship</a>.
    </p>
    <p>
        <a class="btn btn-primary btn-large" href="<?= $this->createUrl('/play') ?>">
            Начать
        </a>
    </p>
</div>
<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <?php
        $myPackage = array(
            'basePath' => 'application.views.assets',
            'css' => array('css/main.css', 'css/game-menu.css'),
            'js' => array('js/main.js',),
            'depends' => array('jquery')
        );

        Yii::app()->assetManager->publish(Yii::getPathOfAlias($myPackage['basePath']), false, -1, defined('YII_DEBUG') && YII_DEBUG);
        Yii::app()->clientScript->addPackage('myPack', $myPackage)->registerPackage('myPack');
        ?>

        <?php Yii::app()->bootstrap->register(); ?>
    </head>

    <body>

        <?php
        $this->widget('bootstrap.widgets.TbNavbar', array(
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'items' => array(
                        array('label' => 'Главная', 'url' => array('/index'), 'active'=>$this->action->id=='index'),
                        array('label' => 'Играть', 'url' => array('/play'), 'active'=>$this->action->id=='play'),
                        array('label' => 'Войти', 'url' => array('/selectLogin'), 'visible' => Yii::app()->user->isGuest, 'active'=>$this->action->id=='selectLogin'),
                        array('label' => 'Выйти (' . Yii::app()->user->name . ')', 'url' => array('logout'), 'visible' => !Yii::app()->user->isGuest)
                    ),
                ),
            ),
        ));
        ?>

        <div class="container" id="page">

            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>

            <div class="clear"></div>
            
            <hr>
            <footer>
                <p><a href="https://me.getid.org">Список других проектов</a>.</p>
            </footer>
        </div><!-- page -->
        
    </body>
</html>


$(function(){
    
    var gc = $(".game")[0];
    var myPlayerIndex;
    var opponent;
    
    $().lobby("bind", "onPlayerIndex", function(playerIndex){
        myPlayerIndex = playerIndex;
        updateTurn(null);
    });
    
    $().lobby("bind", "onOpponent", function(data){
        opponent = data;
        $(".bf-left .bf-desc", gc).html(opponent.name);
    });
    
    $().lobby("bind", "onStarted", function(){
        resetAllStates();
        $().lobby("send", {"action": "getState"});
        $(gc).show();
    });
    
    $().lobby("bind", "onLeftGame", function(){
        
    });
    
    $().lobby("bind", "onSomeOneLeftGame", function(player){
        if (player.uid === opponent.uid){
            $(".game-is-over").show();
            $().lobby("leaveGame");
        }
    });
    
    $().lobby("bind", "onState", function(data){
        if (data.state){
            drawBattlefield(data.leftField, ".enemy-battlefield");
            drawBattlefield(data.rightField, ".my-battlefield");
        }
        updateTurn(data.state);
    });
    
    function drawBattlefield(bfData, parentSelector){
        var html = '<table>';
        for (var i in bfData){
            html += "<tr>";
            for (var j in bfData[i]){
                var s = " ";
                if (bfData[i][j] > 0 && bfData[i][j] < 5){
                    s = "\u25a2"
                } else if (bfData[i][j] == 9){
                    s = "\u25cb"
                } else if (bfData[i][j] == 8){
                    s = "\u25a3"
                } else if (bfData[i][j] == 7){
                    s = "\u2589"
                }
                html += '<td><span data-i="'+i+'" data-j="'+j+'">' + s + "</span></td>";
            }
            html += "</tr>";
        }
        html += "</table>";
        $(gc).find(parentSelector).html(html);
    }
    
    $(".enemy-battlefield td [data-i]", gc).live("click", function(){
        var i = $(this).attr('data-i');
        var j = $(this).attr('data-j');
        $().lobby("send", {"action": "attack", "data": {"i": i, "j": j}});
    });
    
    
    function updateTurn(state) {
        $(".allocate-control").hide();
        switch (state) {
            case null:
                $(".bf-turn", gc).hide();
                $(".game-state-message").hide();
                break;
            case "allocate1":
                if (myPlayerIndex == 1) {
                    $(".allocate-control").show();
                }
                break;
            case "allocate2":
                if (myPlayerIndex == 2) {
                    $(".allocate-control").show();
                }
                break;
            case "turn1":
                if (myPlayerIndex == 1) {
                    $(".bf-enemy-turn").hide();
                    $(".bf-your-turn").show();
                } else if (myPlayerIndex == 2) {
                    $(".bf-enemy-turn").show();
                    $(".bf-your-turn").hide();
                }
                break;
            case "turn2":
                if (myPlayerIndex == 2) {
                    $(".bf-enemy-turn").hide();
                    $(".bf-your-turn").show();
                } else if (myPlayerIndex == 1) {
                    $(".bf-enemy-turn").show();
                    $(".bf-your-turn").hide();
                }
                break;
            default:
                break;
        }
    
    if (state == "win1" || state == "win2"){
        $(".bf-turn", gc).hide();
        $(".game-state-message").hide();
        if (state == "win1" && myPlayerIndex == 1 || state == "win2" && myPlayerIndex == 2){
            $(".you-won").show();
        } else if (myPlayerIndex) {
            $(".you-lose").show();
        } else {
            $(".game-is-over").show();
        }
    }
        
}
    
    $(".allocate-random", gc).click(function(){
        $().lobby("send", {"action": "randomAllocate"});
    });
    
    $(".allocate-ready", gc).click(function(){
        $().lobby("send", {"action": "allocated"});
    });
    
    $().lobby("bind", "onLeftGame", function(){
        $(".game").hide();
        $(".game-is-over").show();
    });
    
    function resetAllStates(){
        $(".bf-turn", gc).hide();
        $(".game-state-message").hide();
        $(".allocate-control").hide();
        $(".game").show();
        $(".battlefield", gc).html("");
    }
});
    
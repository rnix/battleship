(function ($) {
    
    $(function(){
    
        if (typeof webSocketAddress == 'undefined' || typeof siteUid == 'undefined' || typeof siteUserLobbyToken == 'undefined'){
            alert("Ошибка сервера. Зайдите позже.");
            return;
        }
     
        var joinedRoomId;
        var joinedRoom;
        
        $().lobby({
            "wsLobbyAddress": webSocketAddress + "/lobby",
            "uid": siteUid,
            "userLobbyToken": siteUserLobbyToken,
            "wsOnClose": function(){
                $(".connection-wait").hide();
                $(".connection-error").show();
                $(".players-list-container").hide();
                $(".rooms-list-container").hide();
                $(".game-info-container").hide();
                hideChat();
            },
            "wsOnStart": function(){
                $(".connection-wait").hide();
                $(".rooms-list-container").show();
                
            },
            "debug": siteDebugMode
        });
    
        $().lobby("bind", "onRegistered", function(){
            $().lobby("getRoomsList");
        });
        
        $().lobby("bind", "onRoomsList", function(data){
            updateRoomsList(data);
        });
        
        $().lobby("bind", "onJoined", function(data){
            joinedRoomId = data.id;
            joinedRoom = data;
            updatePlayersList(data.members);
            $(".room-name").html(data.name);
            $(".rooms-list-container").hide();
            $(".players-list-container").show();
            if (joinedRoom.ownerId == siteUid){
                $(".start-game-container").show();
            }
            showChat();
        });
        
        $().lobby("bind", "onUpdateRoomMembers", function(members){
            updatePlayersList(members);
        });
    
        $().lobby("bind", "onLeftRoom", function(){
            $(".players-list-container").hide();
            $(".rooms-list-container").show();
            $(".start-game-container").hide();
            $(".game-info-container").hide();
            hideChat();
        });
        
        $().lobby("bind", "onLeftGame", function(data){
            $(".players-list-container").show();
            if (joinedRoom.ownerId == siteUid){
                $(".start-game-container").show();
            }
            $(".game-info-container").hide();
            showChat();
        });
       
        $().lobby("bind", "onStarted", function(data){
            $(".rooms-list-container").hide();
            $(".start-game-container").hide();
            $(".game-info-container").show().find(".game-info-id").html(data.id);
        });
        
        $().lobby("bind", "onChatMessage", function(data){
            var $tpl = $(".room-chat-container .chat-message-tpl").clone();
            $tpl.removeClass("chat-message-tpl");
            $tpl.find(".chat-author").html(data.sender.name);
            $tpl.find(".chat-text").html(data.text);
            $tpl.find(".chat-time").html(data.time);
            $tpl.appendTo($(".chat-lines")).show();
            $(".chat-lines").scrollTop($(".chat-lines")[0].scrollHeight);
        });
        
    
        $(".createRoom").click(function(){
            $().lobby("createRoom", $("input[name='roomName']").val());
        });
    
        $(".leaveRoom").click(function(){
            $().lobby("leaveRoom", joinedRoomId);
        });
        
        $(".startGame").click(function(){
            $().lobby("startGame");
        });
        
        $(".leaveGame").click(function(){
            $().lobby("leaveGame");
        });
        
        $(".exampleAction").click(function(){
            var data = {"action": "exampleAction", "data": "some-data"}
            $().lobby("send", data);
        });
    
        function updateRoomsList(rooms){
            var $list = $(".rooms-list");
            $list.find("li:not(.room-item-tpl)").remove();
            if (!$.isEmptyObject(rooms)){
                $(".empty-rooms-list-message").hide();
                var $tpl = $(".room-item-tpl");
                for(var id in rooms){
                    var room = rooms[id];
                    $tpl.clone().removeClass("room-item-tpl").appendTo($list).attr("data-id", id).show().find(".room-item-name").html(room["name"] + " (" + room["owner"]["name"] + ")");
                }
            } else {
                $(".empty-rooms-list-message").show();
            }
        }
    
        $(".rooms-list li[data-id]").live('click', function(){
            $().lobby("joinRoom", $(this).attr('data-id'));
        });
    
        function updatePlayersList(players){
            var $list = $(".players-list");
            $list.find("li:not(.player-item-tpl)").remove();
            if (!$.isEmptyObject(players)){
                $(".room-is-empty-message").hide();
                var $tpl = $(".player-item-tpl");
                for(var id in players){
                    var player = players[id];
                    $tpl.clone().removeClass("player-item-tpl").appendTo($list).attr("data-id", id).show().find(".player-item-name").html(player["name"]);
                }
            } else {
                $(".room-is-empty-message").show();
            }
            $("body").trigger("player-list-was-updated");
        }
        
        function showChat(){
            $(".room-chat-container").show();
        }
        
        function hideChat(){
            $(".room-chat-container").hide();
        }
        
        function sendChatMessage(){
            var text = $(".room-chat-container .input-chat-text").val();
            if (text){
                $(".room-chat-container .input-chat-text").val('');
                $().lobby("chatMessage", text);
            }
        }
        $(".chat-send").click(sendChatMessage);
        $(document).keypress(function(e) {
            if(e.which == 13) {
                sendChatMessage();
            }
        });
        
        $().lobby("bind", "onError", function(errorMsg){
            $(".on-error-container").html(errorMsg).show();
            setTimeout(function(){
                $(".on-error-container").hide('1000');
            }, 3000);
        });
        
    });

})(jQuery);
<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $uid
 * @property string $name
 * @property string $lll_access_token
 * @property integer $lll_token_expired_in
 * @property string $lll_user_id
 * @property string $ins_date
 * @property string $defeat_num_pve
 * @property string $defeat_num_pvp
 * @property string $victory_num_pve
 * @property string $victory_num_pvp
 * @property string $lobby_reg_token
 * @property string $lobby_reg_token_upd_date
 */
class User extends CActiveRecord {

    protected $livelevelApi;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('uid', 'generateUid', 'on'=>'insert'),
            array('lll_token_expired_in', 'numerical', 'integerOnly' => true),
            array('uid, lll_access_token', 'length', 'max' => 32),
            array('name', 'length', 'max' => 90),
        );
    }

    public function generateUid(){
        $this->uid = md5(microtime(true) . time() .  mt_rand() . '-ANY-SALT-%E&5-' . $this->name);
    }
    
    protected function afterFind(){
        if ($this->lll_access_token){
            $this->livelevelApi = new LiveLevelApi($this->lll_access_token);
        }
    }

    /**
     * Calls API for achieve
     * 
     * @param string $achName - short name of achievement from App params
     */
    public function achieve($achName){
        if ($this->livelevelApi && isset(Yii::app()->params['livelevelAchs'][$achName])){
            $aid = Yii::app()->params['livelevelAchs'][$achName];
            $res = $this->livelevelApi->achieve($aid);
            if (null !== $res){
                $ua = new UserAch;
                $ua->aid = $aid;
                $ua->uid = $this->uid;
                $ua->save();
            }
        }
    }
    
    public function isAchieved($achName){
        return UserAch::model()->exists('uid=:uid AND aid=:aid', array(':uid'=>$this->uid, ':aid'=>Yii::app()->params['livelevelAchs'][$achName]));
    }
    
    public function addWin($pvp = false){
        $prevPvpVictories = $this->victory_num_pvp;
        if ($pvp){
            $this->victory_num_pvp++;
        } else {
            $this->victory_num_pve++;
        }
        $this->save();
        $achName = 'one_victory';
        if (!$this->isAchieved($achName)){
            $this->achieve($achName);
        }
        
    }
    
    public function generateLobbyToken(){
        $this->lobby_reg_token = md5($this->uid . $this->ins_date . time() . rand() . '%h$hI5j@jgj^^FZQFH0jgj4$G');
        $this->lobby_reg_token_upd_date = new CDbExpression('NOW()');
        $this->save();
        return $this->lobby_reg_token;
    }
}
<?php

/**
 * Provides methods for interacting with Livelevel API
 * with access token obtained via OAuth2.
 * 
 */
class LiveLevelApi {

    protected $accessToken;
    protected $apiUrl = "https://livelevel.getid.org/api";

    public function __construct($accessToken) {
        $this->accessToken = $accessToken;
    }

    /**
     * Makes the curl request to the url.
     * @param string $url url to request.
     * @param array $options HTTP request options. Keys: query, data, referer.
     * @param boolean $parseJson Whether to parse response in json format.
     * @return stdClass the response.
     */
    protected function makeRequest($url, $options = array(), $parseJson = true) {
        $ch = $this->initRequest($url, $options);

        $result = curl_exec($ch);
        $headers = curl_getinfo($ch);

        if (curl_errno($ch) > 0)
            throw new CException(curl_error($ch), curl_errno($ch));

        if ($headers['http_code'] != 200) {
            Yii::log(
                    'Invalid response http code: ' . $headers['http_code'] . '.' . PHP_EOL .
                    'URL: ' . $url . PHP_EOL .
                    'Options: ' . var_export($options, true) . PHP_EOL .
                    'Result: ' . $result, CLogger::LEVEL_ERROR, 'application.extensions.eauth'
            );
            throw new CException(Yii::t('livelevel', 'Invalid response http code: {code}.', array('{code}' => $headers['http_code'])), $headers['http_code']);
        }

        curl_close($ch);

        if ($parseJson)
            $result = $this->parseJson($result);

        return $result;
    }

    /**
     * Initializes a new session and return a cURL handle.
     * @param string $url url to request.
     * @param array $options HTTP request options. Keys: query, data, referer.
     * @param boolean $parseJson Whether to parse response in json format.
     * @return cURL handle.
     */
    protected function initRequest($url, $options = array()) {
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // error with open_basedir or safe mode
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); 

        if (isset($options['referer']))
            curl_setopt($ch, CURLOPT_REFERER, $options['referer']);

        if (isset($options['headers']))
            curl_setopt($ch, CURLOPT_HTTPHEADER, $options['headers']);

        if (isset($options['query'])) {
            $url_parts = parse_url($url);
            if (isset($url_parts['query'])) {
                $query = $url_parts['query'];
                if (strlen($query) > 0)
                    $query .= '&';
                $query .= http_build_query($options['query']);
                $url = str_replace($url_parts['query'], $query, $url);
            }
            else {
                $url_parts['query'] = $options['query'];
                $new_query = http_build_query($url_parts['query']);
                $url .= '?' . $new_query;
            }
        }

        if (!empty($options['post'])){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array());
        }
        
        if (isset($options['data'])) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $options['data']);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        return $ch;
    }

    /**
     * Returns the protected resource.
     * @param string $url url to request.
     * @param array $options HTTP request options. Keys: query, data, referer.
     * @param boolean $parseJson Whether to parse response in json format.
     * @return stdClass the response.
     * @see makeRequest
     */
    public function makeSignedRequest($url, $options = array(), $parseJson = true) {
        $options['query']['token'] = $this->accessToken;
        $result = $this->makeRequest($url, $options);
        return $result;
    }
    
    /**
     * Returns the protected resource.
     * @param string $url url to request.
     * @param array $options HTTP request options. Keys: query, data, referer.
     * @param boolean $parseJson Whether to parse response in json format.
     * @return stdClass the response.
     * @see makeRequest
     */
    public function makeSignedPostRequest($url, $options = array(), $parseJson = true) {
        $options['query']['token'] = $this->accessToken;
        $options['post'] = true;
        $result = $this->makeRequest($url, $options);
        return $result;
    }

    /**
     * Parse response from {@link makeRequest} in json format.
     * @param string $response Json string.
     * @return object result.
     */
    protected function parseJson($response) {
        try {
            $result = json_decode($response);
            $error = $this->fetchJsonError($result);
            if (!isset($result)) {
                throw new CException(Yii::t('livelevel', 'Invalid response format.', array()), 500);
            } else if (isset($error) && !empty($error['message'])) {
                throw new CException($error['message'], $error['code']);
            }
            else
                return $result;
        } catch (Exception $e) {
            var_dump($e->getMessage(), $e->getCode());
            //throw new CException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Returns the error info from json.
     * @param stdClass $json the json response.
     * @return array the error array with 2 keys: code and message. Should be null if no errors.
     */
    protected function fetchJsonError($json) {
        if (isset($json->error)) {
            return array(
                'code' => is_string($json->error) ? 0 : $json->error->error_code,
                'message' => is_string($json->error) ? $json->error : $json->error->error_msg,
            );
        } else {
            return null;
        }
    }

    /**
     * 
     * @param int $aid Achievement Id
     * @return type
     */
    public function achieve($aid) {
        $url = $this->apiUrl . "/achievements/achieve/$aid";
        $res = $this->makeSignedPostRequest($url);
        return $res;
    }

}
